import { LoginTypes } from './actions';
import { makeReducerCreator } from '../reduxCreator';

export const initialState = {
  isAuthenticated: !!localStorage.getItem('sessionToken'),
  data: {
    fullName: localStorage.getItem('fullName') || '',
    id: localStorage.getItem('id'),
  },
  roles: '',
  loginError: false,
  loginSuccess: false,
  loading: false,
};

const loginSuccess = (state, { data }) => {
  return {
    ...state,
    data,
    isAuthenticated: true,
    loginError: false,
    loginSuccess: true,
  };
};

const loginFail = (state, action) => {
  return {
    ...state,
    isAuthenticated: false,
    loginError: action.error,
    loginSuccess: false,
  };
};

const logout = () => ({
  isAuthenticated: false,
});

const getCurentUserSuccess = (state, { data }) => {
  return {
    ...state,
    data,
  };
};

const getCurentUserFailure = (state, { error }) => {
  return {
    ...state,
    error,
  };
};

const updateUser = state => {
  return {
    ...state,
    loading: true,
  };
};

const updateUserSuccess = (state, action) => {
  return {
    ...state,
    data: {
      ...state.data,
      ...action.data,
    },
    loading: false,
  };
};

const updateUserFailure = state => {
  return {
    ...state,
    loading: false,
  };
};

export const login = makeReducerCreator(initialState, {
  [LoginTypes.LOGIN_AUTH_SUCCESS]: loginSuccess,
  [LoginTypes.LOGIN_AUTH_FAIL]: loginFail,
  [LoginTypes.LOGOUT]: logout,

  [LoginTypes.GET_CURRENT_USER_SUCCESS]: getCurentUserSuccess,
  [LoginTypes.GET_CURRENT_USER_FAILURE]: getCurentUserFailure,

  [LoginTypes.UPDATE_USER]: updateUser,
  [LoginTypes.UPDATE_USER_SUCCESS]: updateUserSuccess,
  [LoginTypes.UPDATE_USER_FAILURE]: updateUserFailure,
});
