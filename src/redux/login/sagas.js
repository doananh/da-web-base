import { takeEvery, put, call } from 'redux-saga/effects';
import { apiWrapper } from '../reduxCreator';
import {
  LoginTypes,
  loginSuccess,
  loginFailure,
  getCurentUser,
  getCurentUserFailure,
  getCurentUserSuccess,
  updateUserSuccess,
  updateUserFailure,
} from './actions';
import { loginApi, logoutApi, getCurrentUserApi, updateCurrentUserApi } from '../../api/users';

function* loginSaga({ username, password }) {
  try {
    const response = yield call(apiWrapper, loginApi, true, true, username, password);
    if (response.token) {
      localStorage.setItem('sessionToken', response.token);
      yield put(loginSuccess(response));
      yield put(getCurentUser());
    } else {
      yield put(loginFailure(response));
    }
  } catch (error) {
    yield put(loginFailure(error));
  }
}

function* logoutSaga() {
  try {
    yield call(apiWrapper, logoutApi, false, false);
    localStorage.clear('sessionToken');
    localStorage.clear('fullName');
    localStorage.clear('id');
  } catch (error) {
    // /logic here
  }
}

function* getCurrentUserSaga() {
  try {
    const response = yield call(apiWrapper, getCurrentUserApi, true, true, {
      includes: ['medicalInfo', 'userInfo'],
    });
    if (response.id) {
      localStorage.setItem('fullName', response.fullName);
      localStorage.setItem('id', response.id);
      yield put(getCurentUserSuccess(response));
    } else {
      yield put(getCurentUserFailure(response));
    }
  } catch (error) {
    yield put(getCurentUserFailure(error));
  }
}

function* updateUserSaga({ params }) {
  try {
    const response = yield call(apiWrapper, updateCurrentUserApi, true, true, params);
    if (response.id) {
      yield put(updateUserSuccess(response));
    } else {
      yield put(updateUserFailure(response));
    }
  } catch (error) {
    yield put(updateUserFailure(error));
  }
}

export default [
  takeEvery(LoginTypes.LOGIN, loginSaga),
  takeEvery(LoginTypes.LOGOUT, logoutSaga),
  takeEvery(LoginTypes.GET_CURRENT_USER, getCurrentUserSaga),

  takeEvery(LoginTypes.UPDATE_USER, updateUserSaga),
];
