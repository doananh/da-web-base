import { all } from 'redux-saga/effects';
import loginSaga from './login/sagas';
import restSaga from './rest/sagas';
import restFilterSaga from './restFilter/sagas';
import userSaga from './user/sagas';

export default function* root() {
  yield all([...loginSaga, ...restSaga, ...restFilterSaga, ...userSaga]);
}
