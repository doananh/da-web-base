import { call, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import { goBack } from 'react-router-redux';
import { union, keyBy } from 'lodash';
import {
  getList,
  getOneRecord,
  putRecord,
  deleteRecord,
  postRecord,
  batch,
  customQuery,
} from '../../api/restApi';
import * as actions from './actions';
import { apiWrapper } from '../reduxCreator';
import { retrieveReference as _retrieveReference } from './referenceSaga';
import { convertRequestParams, convertResponseData } from './dataProvider';

const { REST_ACTION_TYPES } = actions;

export const retrieveReference = _retrieveReference;

export function* retrieveList({ resource }) {
  try {
    const { limit, skip, filter, include, order } = yield select(state => state.rest[resource]);
    const params = convertRequestParams('getAll', {
      limit: (limit < 1000 && limit) || 10,
      skip: skip || 0,
      filter,
      include,
      order,
    });
    const response = yield call(apiWrapper, getList, false, false, resource, params);
    const convertData = convertResponseData('getAll', response);
    yield put(
      actions.retrieveListSuccess(resource, {
        list: convertData.results,
        ids: convertData.ids,
        count: convertData.count,
      }),
    );
  } catch (error) {
    yield put(actions.retrieveListFailed(resource, error));
  }
}

export function* retrieveOneRecord({ resource, id, data }) {
  try {
    const response = yield call(apiWrapper, getOneRecord, false, false, resource, id, data);
    const rest = yield select(state => state.rest);
    const convertData = convertResponseData('getOne', response);
    const formattedData = {
      list: { [convertData.id]: convertData },
      ids: rest[resource] ? union(rest[resource].ids, [convertData.id]) : [convertData.id],
      count: rest[resource] ? rest[resource].count : 1,
    };
    yield put(actions.retrieveOneRecordSuccess(resource, formattedData));
  } catch (error) {
    yield put(actions.retrieveOneRecordFailed(error));
  }
}

export function* editMultiRecord({ resource, data }) {
  try {
    const requestParams = convertRequestParams('editMulti', data, resource);

    const response = yield call(apiWrapper, batch, true, false, requestParams);
    const convertData = convertResponseData('editMulti', response);

    yield put(
      actions.editRecordSuccess(resource, {
        list: keyBy(convertData.results, 'id'),
      }),
    );
  } catch (error) {
    yield put(actions.editRecordFailed(error));
  }
}

export function* editRecord({ resource, id, data, isGoBack }) {
  try {
    const response = yield call(apiWrapper, putRecord, true, false, resource, id, data);
    const rest = yield select(state => state.rest);
    const convertData = convertResponseData('update', response);
    yield put(
      actions.editRecordSuccess(resource, {
        list: { [convertData.id]: convertData },
        itemLoading: { ...rest[resource].itemLoading, [id]: false },
      }),
    );
    yield put(actions.retrieveOneRecord(resource, id));
    if (!isGoBack) {
      yield put(goBack());
    }
  } catch (error) {
    yield put(actions.editRecordFailed(error, resource, id));
  }
}

export function* createRecord({ resource, data }) {
  try {
    const response = yield call(apiWrapper, postRecord, true, false, resource, data);
    const rest = yield select(state => state.rest);
    const convertData = convertResponseData('create', response);
    yield put(
      actions.createRecordSuccess(resource, {
        list: { [convertData.id]: convertData },
        ids: rest[resource] ? [...rest[resource].ids, convertData.id] : [convertData.id],
        count: rest[resource] ? rest[resource].count + 1 : 1,
      }),
    );
    yield put(actions.retrieveOneRecord(resource, response.id));
    yield put(goBack());
  } catch (error) {
    yield put(actions.createRecordFailed(error));
  }
}

export function* delRecord({ resource, id }) {
  try {
    yield call(apiWrapper, deleteRecord, true, false, resource, id);
    // const rest = yield select(state => state.rest);
    // const newList = _.xorBy(rest[resource].list, [{ id }], 'id');
    // yield put(
    //   actions.retr(resource, {
    //     list: newList,
    //     count: rest[resource].count - 1,
    //   }),
    // );
    yield put(actions.retrieveList(resource));
  } catch (error) {
    yield put(actions.deleteRecordFailed(error));
  }
}

export function* customQuerySaga({ resource, id, data = {}, queryUrl }) {
  try {
    const response = yield call(apiWrapper, customQuery, true, false, resource, id, queryUrl, data);
    const rest = yield select(state => state.rest);
    const convertData = convertResponseData('update', response);
    yield put(
      actions.customQuerySuccess(resource, {
        list: { [convertData.id]: convertData },
        count: rest[resource].count,
        itemLoading: { ...rest[resource].itemLoading, [id]: false },
      }),
    );
    yield put(actions.retrieveOneRecord(resource, id));
  } catch (error) {
    yield put(actions.customQueryFailed(error, resource, id));
  }
}

function restSagas() {
  return [
    takeEvery(REST_ACTION_TYPES.RETRIEVE_LIST, retrieveList),
    takeLatest(REST_ACTION_TYPES.RETRIEVE_ONE_RECORD, retrieveOneRecord),
    takeLatest(REST_ACTION_TYPES.EDIT_MULTI_RECORD, editMultiRecord),
    takeLatest(REST_ACTION_TYPES.EDIT_RECORD, editRecord),
    takeLatest(REST_ACTION_TYPES.DELETE_RECORD, delRecord),
    takeLatest(REST_ACTION_TYPES.CREATE_RECORD, createRecord),
    takeEvery(REST_ACTION_TYPES.RETRIEVE_REFERENCE, retrieveReference),
    takeEvery(REST_ACTION_TYPES.CUSTOM_QUERY_ONE_RECORD, customQuerySaga),
  ];
}

export default restSagas();
