import { takeEvery, put, call } from 'redux-saga/effects';
import { apiWrapper } from '../reduxCreator';
import * as actions from '../rest/actions';
import { UserTypes, getUserDetailSuccess, updateUserSuccess } from './actions';
import { getOneRecord, putRecord } from '../../api/restApi';
import { convertResponseData } from '../rest/dataProvider';

export function* getUserDetail({ data, params }) {
  try {
    const response = yield call(
      apiWrapper,
      getOneRecord,
      true,
      false,
      'users',
      params.userId,
      { includes: ['userInfo', 'medicalInfo'] },
      data,
    );

    const convertData = convertResponseData('getOne', response);
    yield put(getUserDetailSuccess(convertData));
  } catch (error) {
    yield put(actions.retrieveOneRecordFailed(error));
  }
}

export function* updateUserSaga({ id, data }) {
  try {
    yield call(apiWrapper, putRecord, true, false, '/users', id, data);
    yield put(updateUserSuccess(data));
  } catch (error) {
    console.log(error);
  }
}

function userSagas() {
  return [
    takeEvery(UserTypes.GET_USER_DETAIL, getUserDetail),
    takeEvery(UserTypes.UPDATE_USER, updateUserSaga),
  ];
}

export default userSagas();
