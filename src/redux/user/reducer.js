import { UserTypes } from './actions';
import { makeReducerCreator } from '../reduxCreator';

export const initialState = {
  userInfo: {},
  getUserInfoError: null,
};

const getUserDetailSuccess = (state, { data }) => {
  return {
    ...state,
    userInfo: { [data.id]: data },
  };
};

const getUserDetailFailure = (state, action) => {
  return {
    ...state,
    getUserInfoError: action.error,
  };
};

const updateUserSuccess = (state, { data }) => {
  return {
    ...state,
    userInfo: { ...state.userInfo, user: { ...state.userInfo.user, data } },
  };
};

export const users = makeReducerCreator(initialState, {
  [UserTypes.GET_USER_DETAIL_SUCCESS]: getUserDetailSuccess,
  [UserTypes.GET_USER_DETAIL_FAILURE]: getUserDetailFailure,
  [UserTypes.UPDATE_USER_SUCCESS]: updateUserSuccess,
});
