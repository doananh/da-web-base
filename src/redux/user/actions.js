import { makeConstantCreator, makeActionCreator } from '../reduxCreator';

export const UserTypes = makeConstantCreator(
  'GET_USER_DETAIL',
  'GET_USER_DETAIL_SUCCESS',
  'GET_USER_DETAIL_FAILURE',
  'UPLOAD_USER_AVATAR',
  'UPDATE_USER',
  'UPDATE_USER_SUCCESS',
  'UPDATE_USER_FAILURE',
);

export const getUserDetail = params => makeActionCreator(UserTypes.GET_USER_DETAIL, { params });
export const getUserDetailSuccess = data =>
  makeActionCreator(UserTypes.GET_USER_DETAIL_SUCCESS, { data });
export const getUserDetailFailure = error =>
  makeActionCreator(UserTypes.GET_USER_DETAIL_FAILURE, { error });
export const uploadUserAvatar = (id, data) =>
  makeActionCreator(UserTypes.UPLOAD_USER_AVATAR, { id, data });
export const updateUser = (id, data) => makeActionCreator(UserTypes.UPDATE_USER, { id, data });
export const updateUserSuccess = data => makeActionCreator(UserTypes.UPDATE_USER_SUCCESS, { data });
