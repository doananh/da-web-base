import { omitBy, isEmpty, keyBy } from 'lodash';
import { formatMoney } from '../utils/TextUtils';

export const getResourceTitle = string => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export const formatFormData = (originalData, data) => {
  const newData = {};
  Object.keys(data).forEach(key => {
    newData[key] = formatData(data[key], typeof originalData[key]);
  });
  return newData;
};

export const formatData = (data, type) => {
  switch (type) {
    case 'number':
      return Number(data);
    default:
      return data;
  }
};

export const getMatchFromPath = string => {
  const re = '(\\/)((?:[a-z][a-z0-9_]*))(\\/)((?:[a-z][a-z0-9_]*))';
  const p = new RegExp(re, ['i']);
  const m = p.exec(string);
  return m && m.length > 0 ? m[0] : string;
};

export const getSearch = filter => {
  const params = {
    limit: filter.limit,
    skip: filter.skip,
    ...getValidData(filter.filter),
  };

  return Object.keys(params)
    .map(key => {
      return params[key]
        ? `${encodeURIComponent(key)}=${encodeURIComponent(JSON.stringify(params[key]))}`
        : '';
    })
    .filter(data => data !== '')
    .join('&');
};

export const getValidData = filter => {
  return omitBy(filter, item => {
    const sources = item ? Object.keys(item) : [];
    let isInvalid = false;
    sources.forEach(data => {
      if (typeof item === 'object' && isEmpty(getRecordData(item, data))) {
        isInvalid = true;
      }
    });
    return isInvalid;
  });
};

export const getFilterFromUrl = searchStr => {
  const parsed = {};
  if (searchStr.trim() === '') return null;
  decodeURIComponent(searchStr)
    .trim()
    .substring(1)
    .split('&')
    .forEach(text => {
      const keyValue = text.split('=');
      parsed[keyValue[0]] = keyValue[1];
      try {
        parsed[keyValue[0]] = JSON.parse(parsed[keyValue[0]]);
      } catch (error) {
        parsed[keyValue[0]] = parsed[keyValue[0]];
      }
    });
  const filter = { limit: parsed.limit, skip: parsed.skip };
  delete parsed.limit;
  delete parsed.skip;
  filter.filter = parsed;
  return filter;
};

export const getRecordData = (record, source) => {
  const arrKeys = source ? replaceAll(replaceAll(source, '\\[', '.'), '\\]', '').split('.') : [];
  let data = record;
  arrKeys.forEach(key => {
    data = data ? data[key] : data;
  });
  return data;
};

export const convertDataToObj = (formatOnSubmit, record) => {
  const newRecord = {};
  Object.keys(record).forEach(key => {
    newRecord[key] = formatOnSubmit[key]
      ? { ...record[key], ...formatOnSubmit[key](record[key]) }
      : record[key];
  });
  // const arrKeys = source.split('.');
  // let data = record;
  // arrKeys.forEach((key, index) => {
  //   if (index === arrKeys.index - 1) {
  //     data[key] = value;
  //   } else {
  //     data = data[key];
  //   }
  // });
  return newRecord;
};

export const replaceAll = function(str, search, replacement) {
  return str.replace(new RegExp(search, 'g'), replacement);
};

export const formattedRESTData = data => {
  return {
    data: keyBy(data),
    ids: data.map(item => item.id),
  };
};

export const getIdByUrl = props => {
  return props.route
    ? props.route
        .substring(
          props.route.indexOf(`${props.resource}/`),
          props.route.lastIndexOf('/edit'),
        )
        .replace(`${props.resource}/`, '')
    : props.match.params.id;
};

export const getPrefixPath = (props, action) => {
  return `${
    props.redirects[action] === 'modal'
      ? `${props.location.pathname}${props.location.search}#`
      : props.rootPath
  }/${props.resource}`;
};

export const getTotalValue = (list, valueKey) => {
  if (!list) return 0;
  let total = 0;
  list.forEach(e => {
    total += e[valueKey];
  });
  return formatMoney(total);
};
