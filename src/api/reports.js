import { get } from './utils';

export async function getCustomerReports() {
  return get('/customers/summary');
}
