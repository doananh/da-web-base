import { post, get, put } from './utils';

export async function loginApi(email, password) {
  return post('/auth/login', { email, password });
}

export async function logoutApi() {
  return post('/auth/logout');
}

export async function getCurrentUserApi(filter) {
  return get('/auth/me', filter);
}

export async function updateCurrentUserApi(data) {
  return put('/auth/me', data);
}
export async function filterUser(filter, offset, limit) {
  return get('/users', { filter: JSON.stringify(filter), offset, limit });
}
