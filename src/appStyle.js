import styled from 'styled-components';
import { palette, font } from 'styled-theme';

const DashAppHolder = styled.div`
  body {
  }
  font-family: ${font('primary', 0)};

  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  a,
  p,
  li,
  input,
  textarea,
  span,
  div,
  img,
  svg {
    &::selection {
      background: ${palette('primary', 0)};
      color: #fff;
    }
  }

  .ant-row:not(.ant-form-item) {
    ${'' /* margin-left: -8px;
    margin-right: -8px; */};
    &:before,
    &:after {
      display: none;
    }
  }

  .ant-row > div {
    ${'' /* padding: 0;
    padding: 0; */};
  }

  .isoLeftRightComponent {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
  }

  .isoCenterComponent {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
  }

  .isoLayoutContentWrapper {
    width: 100%;
    padding: 30px 20px;
  }
  .anticon:before {
    display: block;
    font-family: 'anticon', 'CSM-Web' !important;
  }
  .anticon:after {
    display: block;
    font-family: 'anticon', 'CSM-Web' !important;
  }

  .ant-input,
  .ant-select-selection,
  .ant-input-number,
  .ant-select-clear-icon,
  textarea {
    background: ${palette('background', 2)};
    border: 1px solid ${palette('background', 2)};
    &:hover,
    &:focus,
    &:active {
      border: 1px solid ${palette('border', 0)};
    }
  }

  .ant-pagination-item:hover {
    background-color: ${palette('table', 4)};
    border-color: ${palette('table', 4)};
    a {
      color: ${palette('table', 5)};
    }
  }
  .ant-pagination-item {
    border: 0px;
    color: ${palette('text', 1)};
    a {
      color: ${palette('text', 1)};
      text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
      font-size: 14px;
    }
  }
  .ant-pagination-prev .ant-pagination-item-link,
  .ant-pagination-next .ant-pagination-item-link,
  .ant-pagination-item-link-icon,
  .ant-pagination-item-container {
    border-color: ${palette('border', 0)};
    color: ${palette('text', 1)};
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
    font-size: 14px;
  }
  .ant-pagination-item-ellipsis {
    color: ${palette('text', 1)} !important;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
    font-size: 14px;
  }
  .ant-pagination-item-active {
    background-color: ${palette('table', 4)};
    border-color: ${palette('table', 4)};
    color: ${palette('table', 5)};
    a {
      color: ${palette('table', 5)};
    }
  }
  .ant-select-arrow-icon {
  }

  .ant-pagination-options-quick-jumper input {
    background: ${palette('table', 1)};
  }
  .ant-pagination-options-size-changer {
    .ant-select-selection--single,
    .ant-select-dropdown-menu,
    .ant-select-dropdown-menu-item {
      background: ${palette('table', 1)};
    }
  }

  .ant-table-thead > tr > th {
    font-weight: 500;
    font-size: 14px;
    color: ${palette('text', 0)};
    border-bottom: 1px solid ${palette('border', 0)};
  }

  .ant-table-placeholder {
    background: ${palette('table', 1)};
    border-bottom: 1px solid ${palette('border', 0)};
    color: ${palette('text', 0)};
  }

  .ant-switch {
    background: rgba(0, 0, 0, 0.12);
  }
  .ant-switch-checked {
    background-color: ${palette('primary', 0)};
  }
  * {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  div::-webkit-scrollbar-thumb {
    border-radius: 3px !important;
    background: ${palette('secondary', 2)} !important;
    box-shadow: 1px 1px 2px 0 rgba(0, 0, 0, 0.5) !important;
  }
  div::-webkit-scrollbar-track {
    background: ${palette('grayscale', 2)} !important;
  }
  div::-webkit-scrollbar-thumb:hover {
    border-radius: 3px !important;
    background: ${palette('secondary', 2)} !important;
    box-shadow: 1px 1px 2px 0 rgba(0, 0, 0, 0.5) !important;
  }
  div::-webkit-scrollbar {
    width: 6px;
    background: ${palette('grayscale', 2)} !important;
  }
  .ant-table-row {
    background: ${palette('table', 1)};
  }
  .ant-table-tbody > tr > td {
    border-bottom: 1px solid ${palette('secondary', 1)};
    color: ${palette('text', 0)};
  }
  .ant-table-row:hover {
    & > td {
    }
  }
  .ant-table-row:focus {
    background: ${palette('table', 2)};
  }
  .ant-tabs-bar {
    border-bottom: none;
  }
  .ant-tabs-tab {
    color: ${palette('text', 5)};
  }
  input:-webkit-autofill,
  input:-webkit-autofill:hover,
  input:-webkit-autofill:focus textarea:-webkit-autofill,
  textarea:-webkit-autofill:hover textarea:-webkit-autofill:focus,
  select:-webkit-autofill,
  select:-webkit-autofill:hover,
  select:-webkit-autofill:focus {
    border: none;
    -webkit-text-fill-color: white;
    -webkit-box-shadow: 0 0 0px 1000px #000 inset;
    transition: background-color 5000s ease-in-out 0s;
  }
`;

export default DashAppHolder;
