import excel from './images/icon/ic_excel.png';
import edit from './images/icon/ic-edit.png';
import search from './images/icon/ic_search.png';
import notification from './images/icon/ic_notifications.png';

import unselectDashboard from './images/icon/unselect/ic_dashboard.png';
import unselectContact from './images/icon/unselect/ic_contact.png';
import unselectCompany from './images/icon/unselect/ic_company.png';
import unselectCampaigns from './images/icon/unselect/ic_campaign.png';
import unselectReport from './images/icon/unselect/ic_report.png';
import unselectEmployee from './images/icon/unselect/ic_people.png';
import unselectSetting from './images/icon/unselect/ic_setting.png';

import dashboard from './images/icon/selected/ic_dashboard.png';
import contact from './images/icon/selected/ic_contact.png';
import company from './images/icon/selected/ic_company.png';
import campaigns from './images/icon/selected/ic_campaign.png';
import report from './images/icon/selected/ic_report.png';
import employee from './images/icon/selected/ic_people.png';
import setting from './images/icon/selected/ic_setting.png';

export const Images = {
  icon: {
    sidebar: {
      unselect: {
        dashboard: unselectDashboard,
        contact: unselectContact,
        company: unselectCompany,
        campaigns: unselectCampaigns,
        report: unselectReport,
        employee: unselectEmployee,
        setting: unselectSetting,
      },
      selected: {
        dashboard,
        contact,
        company,
        campaigns,
        report,
        employee,
        setting,
      },
    },
    rest: {
      excel,
      edit,
      search,
    },
    header: {
      notification,
    },
  },
};
