import React from 'react';
import PropTypes from 'prop-types';
import { IconWrapper } from './styles';

const icLogo = () => (
  <span className="icon-ic-logo">
    <span className="path1" />
    <span className="path2" />
    <span className="path3" />
  </span>
);

const icNotifications = () => (
  <span className="icon-ic-notifications">
    <span className="path1" />
    <span className="path2" />
  </span>
);

const icEdit = () => (
  <span className="icon-ic-edit">
    <span className="path1" />
    <span className="path2" />
  </span>
);

const icExcel = () => (
  <span className="icon-ic-excel">
    <span className="path1" />
    <span className="path2" />
    <span className="path3" />
    <span className="path4" />
    <span className="path5" />
    <span className="path6" />
    <span className="path7" />
    <span className="path8" />
    <span className="path9" />
    <span className="path10" />
  </span>
);

const icLogoText = () => (
  <span className="icon-logo">
    <span className="path1" />
    <span className="path2" />
    <span className="path3" />
    <span className="path4" />
  </span>
);

const icDashboard = () => (
  <span className="icon-ic-dashboard">
    <span className="path1" />
    <span className="path2" />
    <span className="path3" />
    <span className="path4" />
  </span>
);

const icPeople = () => (
  <span className="icon-ic-people">
    <span className="path1" />
    <span className="path2" />
  </span>
);

const icReport = () => (
  <span className="icon-ic-report">
    <span className="path1" />
    <span className="path2" />
    <span className="path3" />
    <span className="path4" />
  </span>
);

const icSetting = () => (
  <span className="icon-ic-setting">
    <span className="path1" />
    <span className="path2" />
  </span>
);

const icCompaign = () => (
  <span className="icon-ic-campaign">
    <span className="path1" />
    <span className="path2" />
  </span>
);

const icCompany = () => (
  <span className="icon-ic-company">
    <span className="path1" />
    <span className="path2" />
  </span>
);

const icContact = () => (
  <span className="icon-ic-contact">
    <span className="path1" />
    <span className="path2" />
    <span className="path3" />
    <span className="path4" />
  </span>
);

const ICONS = {
  'ic-logo': icLogo,
  'ic-notifications': icNotifications,
  'ic-edit': icEdit,
  'ic-excel': icExcel,
  logo: icLogoText,
  'ic-dashboard': icDashboard,
  'ic-people': icPeople,
  'ic-report': icReport,
  'ic-setting': icSetting,
  'ic-campaign': icCompaign,
  'ic-company': icCompany,
  'ic-contact': icContact,
};

const Icomoon = ({ type, className, size = 14 }) => {
  const Icon = ICONS[type];
  return (
    <IconWrapper className={className} style={{ fontSize: size }}>
      <Icon />
    </IconWrapper>
  );
};

Icomoon.propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.number,
};

export default Icomoon;
