import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import Loadable from 'react-loadable';
import { Tabs } from 'antd';
import { push } from 'react-router-redux';
import LayoutWrapper from '../../components/utility/LayoutWrapper';
import PageHeader from '../../components/utility/PageHeader';
import IntlMessages from '../../components/utility/IntlMessages';
// import Loading from '../../components/common/LoadingScreen';
import ConfigWrapper from './style';

const TabPane = Tabs.TabPane;

const CONFIG_TABS = [
  // {
  //   title: 'users',
  //   key: 'users',
  //   PaneComponent: Loadable({
  //     loader: () => import('../Users/List'),
  //     loading: Loading,
  //   }),
  // },
];

class ConfigPage extends Component {
  static propTypes = {
    match: PropTypes.object,
    pushQuery: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      activeKey: this.props.match.params.resource || CONFIG_TABS[0].key,
    };
  }

  onChange = activeKey => {
    this.updateTabUI(this.state.panes, activeKey);
  };

  updateTabUI = (panes, activeKey) => {
    this.setState({ panes, activeKey });
    this.props.pushQuery(activeKey);
  };

  render() {
    return (
      <LayoutWrapper>
        <PageHeader>
          <IntlMessages id="page.config" />
        </PageHeader>
        <ConfigWrapper>
          <Tabs
            className="tabs"
            onChange={this.onChange}
            activeKey={this.state.activeKey}
            defaultActiveKey={this.props.match.params.resource}
          >
            {CONFIG_TABS.map(({ title, key, closable, PaneComponent }) => (
              <TabPane tab={<IntlMessages id={title} />} key={key} closable={closable}>
                <div>
                  <PaneComponent {...this.props} rootPath="/config" />
                </div>
              </TabPane>
            ))}
          </Tabs>
        </ConfigWrapper>
      </LayoutWrapper>
    );
  }
}

const mapStateToProps = () => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    pushQuery: resource => dispatch(push(`/config/${resource}`)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ConfigPage);
