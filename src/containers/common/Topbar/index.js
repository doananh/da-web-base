import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTheme } from 'styled-components';
import { connect } from 'react-redux';
import { Layout } from 'antd';
import appActions from '../../../redux/app/actions';
import TopbarWrapper from './style';
import { TopbarUser, TopbarNotification } from '../../../components/common/Topbar';
import { logout as logoutAction } from '../../../redux/login/actions';
import { showModal as showModalAction } from '../../../redux/modal/actions';

const { Header } = Layout;

class Topbar extends Component {
  state = {
    selectedItem: '',
  };

  showModal = route => () => {
    const { showModal } = this.props;
    showModal(route);
  };

  render() {
    const { locale, logout, userInfo, theme } = this.props;
    // const collapsed = this.props.collapsed && !openDrawer;
    const styling = {
      background: theme.palette.background[1],
      width: '100%',
      height: 64,
      position: 'fixed',
    };
    return (
      <TopbarWrapper>
        <Header style={styling} className={'isomorphicTopbar collapsed'}>
          <div className="isoLeft" />
          <ul className="isoRight">
            <li
              onClick={() => {
                this.setState({ selectedItem: 'user' });
              }}
              className="isoUser"
            >
              <TopbarUser userInfo={userInfo} logout={logout} locale={locale} />
            </li>
            <li className="isoNotify">
              <TopbarNotification />
            </li>
          </ul>
        </Header>
      </TopbarWrapper>
    );
  }
}

Topbar.propTypes = {
  locale: PropTypes.string,
  logout: PropTypes.func,
  userInfo: PropTypes.object,
  theme: PropTypes.object,
  showModal: PropTypes.func,
};

const { toggleCollapsed } = appActions;

export default connect(
  state => ({
    ...state.App,
    userInfo: state.login.data,
    locale: state.LanguageSwitcher.language.locale,
  }),
  dispatch => ({
    toggleCollapsed: () => {
      dispatch(toggleCollapsed());
    },
    logout: () => dispatch(logoutAction()),
    showModal: route => dispatch(showModalAction(route)),
  }),
)(withTheme(Topbar));
