import styled from 'styled-components';
import { Layout } from 'antd';
import { palette } from 'styled-theme';

export const FooterWrapper = styled(Layout.Footer)`
  background: ${palette('background', 1)} !important;
  text-align: center;
  border-top: 1px solid ${palette('border', 0)} !important;
  color: ${palette('text', 0)};
`;
