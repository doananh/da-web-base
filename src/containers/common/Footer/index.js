import React from 'react';
import PropTypes from 'prop-types';
import { FooterWrapper } from './style';

const Footer = props => {
  return <FooterWrapper>{props.children}</FooterWrapper>;
};
Footer.propTypes = {
  children: PropTypes.node,
};

export default Footer;
