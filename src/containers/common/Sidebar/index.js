import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Layout, Menu, Icon } from 'antd';
import clone from 'clone';
import { Scrollbars } from 'react-custom-scrollbars';
import IntlMessages from '../../../components/utility/IntlMessages';
import { getCurrentTheme } from '../ThemeSwitcher/config';
import SidebarWrapper from './style';
import appActions from '../../../redux/app/actions';
import Logo from '../../../components/utility/Logo';
import { rtl } from '../../../config/withDirection';
import Icomoon from '../../../assets/icomoon';

const { Sider } = Layout;
const customizedTheme = getCurrentTheme('sidebarTheme', 'themedefault');
const stripTrailingSlash = str => {
  if (str.substr(-1) === '/') {
    return str.substr(0, str.length - 1);
  }
  return str;
};
const listMenu = [
  {
    key: 'dashboard',
    url: '/',
    icon: 'ic-dashboard',
    text: 'sidebar.dashboard',
  },
  {
    key: 'setting',
    url: '/setting/leadStatuses',
    icon: 'ic-setting',
    text: 'sidebar.setting',
  },
];

class Sidebar extends Component {
  getAncestorKeys = key => {
    const map = {
      sub3: ['sub2'],
    };
    return map[key] || [];
  };

  renderView({ style, ...props }) {
    const viewStyle = {
      marginRight: rtl === 'rtl' ? '0' : '-25px',
      paddingRight: rtl === 'rtl' ? '0' : '9px',
      marginLeft: rtl === 'rtl' ? '-17px' : '0',
      paddingLeft: rtl === 'rtl' ? '9px' : '0',
    };
    return <div className="box" style={{ ...style, ...viewStyle }} {...props} />;
  }

  render() {
    const { app, toggleCollapsed } = this.props;
    const url = stripTrailingSlash('/');
    const collapsed = clone(app.collapsed) && !clone(app.openDrawer);
    const mode = collapsed === true ? 'vertical' : 'inline';
    console.log('app.current', app.current);
    const scrollheight = app.height;
    const styling = {
      backgroundColor: customizedTheme.backgroundColor,
    };
    const submenuColor = {
      color: customizedTheme.textColor,
    };
    return (
      <SidebarWrapper>
        <Sider
          trigger={null}
          collapsible
          collapsed={collapsed}
          width="240"
          className="isomorphicSidebar"
          collapsedWidth={64}
          style={styling}
        >
          <Scrollbars renderView={this.renderView} style={{ height: scrollheight }}>
            <div className="vButtonDrawer">
              <div className="row" style={submenuColor}>
                <Logo collapsed={collapsed} onClick={toggleCollapsed} />
              </div>
            </div>
            <Menu
              theme="dark"
              mode={mode}
              openKeys={collapsed ? [] : app.openKeys}
              defaultSelectedKeys={app.current.length > 0 ? app.current : ['dashboard']}
              className="isoDashboardMenu"
            >
              {listMenu.map(menu => {
                return (
                  <Menu.Item key={menu.key}>
                    <Link to={`${url}${menu.url}`}>
                      <span className="isoMenuHolder" style={submenuColor}>
                        <Icomoon type={menu.icon} className="anticon sidebarIcon" />
                        <span className="nav-text">
                          <IntlMessages id={menu.text} />
                        </span>
                      </span>
                    </Link>
                  </Menu.Item>
                );
              })}
            </Menu>
          </Scrollbars>
          <div role="button" onClick={toggleCollapsed} className="buttonDrawer">
            <div className="buttonDrawerBackground" />
            <Icon type={collapsed ? 'double-right' : 'double-left'} className="iconDrawer" />
          </div>
        </Sider>
      </SidebarWrapper>
    );
  }
}

Sidebar.propTypes = {
  app: PropTypes.object,
  toggleCollapsed: PropTypes.func,
};

const { toggleCollapsed } = appActions;

export default connect(
  state => ({
    app: state.App,
  }),
  dispatch => ({
    toggleCollapsed: () => {
      dispatch(toggleCollapsed());
    },
  }),
)(Sidebar);
