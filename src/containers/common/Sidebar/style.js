import styled from 'styled-components';
import { palette } from 'styled-theme';
import { transition, borderRadius } from '../../../config/style-util';
import WithDirection from '../../../config/withDirection';

const SidebarWrapper = styled.div`
  .vButtonDrawer {
    height: 64px;
    width: 100%;
    background: ${palette('sidebar', 0)} !important;
    display: flex;
    align-items: center;
    overflow: hidden;
    .appName {
      font-size: 20px !important;
      color: ${palette('primary', 0)} !important;
      width: 240px;
    }
    .appNameBold {
      font-size: 20px !important;
      color: white !important;
      font-weight: bold;
      width: 100px;
    }
  }
  .buttonDrawer {
    border: 0;
    margin-left: 5px;
    &:hover {
      border: 0;
    }
    &:active {
      border: 0;
    }
    &:focus {
      border: 0;
    }
  }
  .buttonDrawerBackground {
    background: ${palette('sidebar', 0)} !important;
    width: 30px;
    height: 30px;
    position: absolute;
    transform: rotate(45deg);
    border-top: 1px solid ${palette('border', 0)};
    border-top-right-radius: 15px;
  }
  .iconDrawer {
    color: ${palette('primary', 0)};
    z-index: 1;
    font-size: 14px !important;
    margin-left: 5px !important;
    @media only screen and (max-width: 767px) {
      font-size: 10px !important;
      margin-left: 10px !important;
    }
  }
  .footerSider {
    background: ${palette('sidebar', 0)} !important;
    height: 70px;
    width: 100%;
    padding-bottom: 20px;
    display: flex;
    align-items: flex-end;
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    .anticon {
      font-size: 18px;
      margin-right: 30px;
      color: ${palette('sidebar', 2)};
    }

    .txtLogout {
      font-size: 14px;
      color: ${palette('sidebar', 2)};
      font-weight: 400;
      ${transition()};
    }
  }
  .btnLogout {
    background: transparent;
    display: flex;
    align-items: center;
    margin-left: 5px;
    border: 0;
    &:hover {
      background: transparent;
    }
  }

  .isomorphicSidebar {
    z-index: 1000;
    background: ${palette('sidebar', 0)} !important;
    border-right: 1px solid ${palette('border', 0)};
    width: 280px;
    flex: 0 0 280px;

    @media only screen and (max-width: 767px) {
      width: 240px !important;
      flex: 0 0 240px !important;
    }

    &.ant-layout-sider-collapsed {
      @media only screen and (max-width: 767px) {
        width: 0;
        min-width: 0 !important;
        max-width: 0 !important;
        flex: 0 0 0 !important;
      }
    }

    .isoLogoWrapper {
      height: 70px;
      background: rgba(0, 0, 0, 0.3);
      margin: 0;
      padding: 0 10px;
      margin-right: 10px;
      text-align: center;
      overflow: hidden;
      ${borderRadius()};

      h3 {
        a {
          font-size: 21px;
          font-weight: 300;
          line-height: 70px;
          letter-spacing: 3px;
          text-transform: uppercase;
          color: ${palette('grayscale', 6)};
          display: block;
          text-decoration: none;
        }
      }
    }

    &.ant-layout-sider-collapsed {
      .isoLogoWrapper {
        padding: 0;
        margin-right: 10px;

        h3 {
          a {
            font-size: 27px;
            font-weight: 500;
            letter-spacing: 0;
          }
        }
      }
    }

    .isoDashboardMenu {
      padding-bottom: 35px;
      background: transparent;

      a {
        text-decoration: none;
        font-weight: 400;
      }

      .ant-menu-item {
        width: 100%;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding: 0 24px !important;
        &:not(.ant-menu-item-selected) {
          .icon-ic-setting span:before,
          .icon-ic-report span:before,
          .icon-ic-people span:before,
          .icon-ic-dashboard span:before,
          .icon-ic-contact span:before,
          .icon-ic-company span:before,
          .icon-ic-campaign span:before {
            color: ${palette('grayscale', 0)};
          }
        }
      }

      .isoMenuHolder {
        display: flex;
        align-items: center;
      }

      .sidebarIcon {
        font-size: 18px;
        margin-right: 30px;
        color: ${palette('sidebar', 2)};
        ${transition()};
      }
      .anticon {
        font-size: 18px;
        margin-right: 30px;
        color: ${palette('sidebar', 2)};
        ${transition()};
      }

      i {
        font-size: 19px;
        color: ${palette('sidebar', 2)};
        margin: ${props => (props['data-rtl'] === 'rtl' ? '0 0 0 30px' : '0 30px 0 0')};
        width: 18px;
        ${transition()};
      }

      .nav-text {
        font-size: 14px;
        color: ${palette('sidebar', 2)};
        font-weight: 400;
        ${transition()};
      }

      .ant-menu-item-selected {
        background: ${palette('sidebar', 1)} !important;
        border-radius: 0;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        .sidebarIcon {
          font-size: 18px;
          margin-right: 28px;
          color: ${palette('sidebar', 2)};
          ${transition()};
        }
        .anticon {
          background-repeat: no-repeat;
          color: ${palette('sidebar', 3)};
          font-weight: bold;
        }

        i {
          color: ${palette('sidebar', 3)};
          font-weight: bold;
        }

        .nav-text {
          color: ${palette('sidebar', 3)};
          font-weight: bold;
        }
      }

      > li {
        &:hover {
          i,
          .nav-text {
            color: ${palette('sidebar', 4)};
          }
        }
      }
    }

    .ant-menu-dark .ant-menu-inline.ant-menu-sub {
      background: ${palette('sidebar', 1)} !important;
    }

    .ant-menu-submenu-inline,
    .ant-menu-submenu-vertical {
      > .ant-menu-submenu-title {
        width: 100%;
        display: flex;
        align-items: center;
        padding: 0 24px;

        > span {
          display: flex;
          align-items: center;
        }

        &:after {
          content: '\f123';
          font-family: 'Ionicons' !important;
          font-size: 16px;
          color: ${palette('sidebar', 2)};
          left: ${props => (props['data-rtl'] === 'rtl' ? '16px' : 'auto')};
          right: ${props => (props['data-rtl'] === 'rtl' ? 'auto' : '16px')};
          ${transition()};
        }

        &:hover {
          &:after {
            color: ${palette('sidebar', 3)};
          }
        }
      }

      .ant-menu-inline,
      .ant-menu-submenu-vertical {
        > li:not(.ant-menu-item-group) {
          padding-left: ${props =>
            props['data-rtl'] === 'rtl' ? '0px !important' : '74px !important'};
          padding-right: ${props =>
            props['data-rtl'] === 'rtl' ? '74px !important' : '0px !important'};
          font-size: 13px;
          font-weight: 400;
          color: ${palette('sidebar', 2)};
          ${transition()};

          &:hover {
            color: ${palette('sidebar', 3)};
          }
        }

        .ant-menu-item-group {
          padding-left: 0;

          .ant-menu-item-group-title {
            padding-left: 100px !important;
          }
          .ant-menu-item-group-list {
            .ant-menu-item {
              padding-left: 125px !important;
            }
          }
        }
      }

      .ant-menu-sub {
        background-color: transparent !important;
      }
    }

    &.ant-layout-sider-collapsed {
      .nav-text {
        display: none;
      }

      .ant-menu-submenu-inline > {
        .ant-menu-submenu-title:after {
          display: none;
        }
      }

      .ant-menu-submenu-vertical {
        > .ant-menu-submenu-title:after {
          display: none;
        }

        .ant-menu-sub {
          background-color: transparent !important;

          .ant-menu-item {
            height: 35px;
          }
        }
      }
    }
    .row {
      display: flex;
      align-items: center;
    }
  }
`;

export default WithDirection(SidebarWrapper);
