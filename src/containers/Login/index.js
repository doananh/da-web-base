import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form } from 'antd';
import Checkbox from '../../components/uielements/Checkbox';
import Button from '../../components/uielements/Button';
import { login as loginAction } from '../../redux/login/actions';
import IntlMessages from '../../components/utility/IntlMessages';
import SignInStyleWrapper from './style';
import RestFormInput from '../../components/form/FormInput';
import Text from '../../components/common/Text';
import Icomoon from '../../assets/icomoon';

class SignIn extends Component {
  constructor(props) {
    const { isAuthenticated } = props;
    super(props);
    this.state = {
      redirectToReferrer: isAuthenticated,
    };
  }

  componentWillReceiveProps(nextProps) {
    const { isAuthenticated } = this.props;
    if (isAuthenticated !== nextProps.isAuthenticated && nextProps.isAuthenticated === true) {
      this.setState({ redirectToReferrer: true });
    }
  }

  handleLogin = e => {
    e.preventDefault();
    const { form, login } = this.props;
    form.validateFields((err, values) => {
      if (!err && values) {
        const { username, password } = values;
        login(username, password);
      }
    });
  };

  render() {
    const from = { pathname: '/dashboard' };
    const { redirectToReferrer } = this.state;
    const { form } = this.props;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    return (
      <SignInStyleWrapper className="isoSignInPage">
        <div className="isoLoginContentWrapper">
          <div className="isoLoginContent">
            <div className="isoLogoWrapper">
              <Link to="/">
                <Icomoon type="logo" size={40} />
              </Link>
            </div>
            <Text type="h3" align="center">
              <IntlMessages id="auth.login" />
            </Text>
            <div className="isoSignInForm">
              <Form onSubmit={this.handleLogin}>
                <div className="isoInputWrapper">
                  <RestFormInput
                    form={form}
                    title={<IntlMessages id="auth.email" />}
                    source="username"
                    rules={[
                      {
                        required: true,
                        message: <IntlMessages id="auth.email.message" />,
                      },
                    ]}
                    defaultValue="superadmin@oz.com"
                  />
                </div>

                <div className="isoInputWrapper">
                  <RestFormInput
                    form={form}
                    source="password"
                    type="password"
                    title={<IntlMessages id="auth.password" />}
                    rules={[
                      {
                        required: true,
                        message: <IntlMessages id="auth.password.message" />,
                      },
                    ]}
                    defaultValue="abc123"
                  />
                </div>
                <div className="isoInputWrapper isoLeftRightComponent">
                  <Checkbox className="isoRemindText">
                    <IntlMessages id="page.signInRememberMe" />
                  </Checkbox>
                  <Button type="primary" htmlType="submit" onClick={this.handleLogin}>
                    <IntlMessages id="page.signInButton" />
                  </Button>
                </div>
              </Form>
              <div className="isoCenterComponent isoHelperWrapper">
                <Link to="/auth/forgot-password" className="isoForgotPass">
                  <IntlMessages id="page.signInForgotPass" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </SignInStyleWrapper>
    );
  }
}

SignIn.propTypes = {
  isAuthenticated: PropTypes.bool,
  login: PropTypes.func,
  form: PropTypes.object,
};

const WrappedSignInForm = Form.create()(SignIn);

export default connect(
  state => ({
    isAuthenticated: state.login.isAuthenticated,
  }),
  dispatch => ({
    login: (username, password) => {
      dispatch(loginAction(username, password));
    },
  }),
)(WrappedSignInForm);
