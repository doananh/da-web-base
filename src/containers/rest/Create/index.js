import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { goBack as goBackAction } from 'react-router-redux';
import { createRecord } from '../../../redux/rest/actions';
import RestCreateComponent from '../../../components/RestLayout/Create';
import IntlMessages from '../../../components/utility/IntlMessages';
import { getFilterFromUrl } from '../../../helpers/Tools';
import Text from '../../../components/common/Text';
import { closeModal as closeModalAction } from '../../../redux/modal/actions';

class RestCreate extends Component {
  onBack = () => {
    const { route, closeModal, goBack } = this.props;
    if (!route) {
      goBack();
    } else {
      closeModal();
    }
  };

  render() {
    const { showModal, title, resource } = this.props;
    return !showModal ? (
      <RestCreateComponent {...this.props} onBack={this.onBack} />
    ) : (
      <div>
        <Text type="h4White" className="modalTitle">
          {!title || typeof title === 'string' ? <IntlMessages id={title || resource} /> : title}
        </Text>
        <RestCreateComponent {...this.props} onBack={this.onBack} />
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  const defaultValues = decodeURI(props.location.search.substring(1)).trim();
  return {
    route: state.modal.current,
    record: defaultValues !== '' ? getFilterFromUrl(defaultValues).filter : {},
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onSubmit: data => dispatch(createRecord(props.resource, data, props.gotoShowPage)),
    gotoShowPage: id => props.history.push(`${props.match.path.replace('create', '')}/${id}/edit`),
    closeModal: () => dispatch(closeModalAction()),
    goBack: () => dispatch(goBackAction()),
  };
};

RestCreate.propTypes = {
  closeModal: PropTypes.func,
  resource: PropTypes.string,
  title: PropTypes.any,
  route: PropTypes.string,
  showModal: PropTypes.bool,
  goBack: PropTypes.func,
};
const ConnectedRestCreate = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RestCreate);

ConnectedRestCreate.propTypes = {
  goShowPageWhenSuccess: PropTypes.bool,
};
ConnectedRestCreate.defaultProps = {
  goShowPageWhenSuccess: true,
};
export default ConnectedRestCreate;
