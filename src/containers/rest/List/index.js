import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { retrieveList, editRecord, deleteRecord, customQuery } from '../../../redux/rest/actions';
import RestListComponent from '../../../components/RestLayout/List';
import { getFilterFromUrl, getSearch } from '../../../helpers/Tools';
import { getLoading, getFilters, getResources } from '../../../redux/rest/selectors';
import { showModal as showModalAction } from '../../../redux/modal/actions';

class RestList extends Component {
  constructor(props) {
    super(props);
    const filter =
      (this.props.location && getFilterFromUrl(this.props.location.search)) ||
      this.props.initialFilter;
    this.props.retrieveList(filter || { limit: 10, skip: 0, filter: {} }, true);
  }

  retrieveList = filter => {
    this.props.pushQuery(filter);
  };

  gotoEditPage = id => {
    const { redirects, pushRoute, showModal, resource, rootPath } = this.props;
    const route = `${rootPath}/${resource}/${id}/edit`;
    if (redirects.edit === 'modal') {
      showModal(route);
    } else {
      pushRoute(route);
    }
  };

  gotoShowPage = id => {
    const { redirects, pushRoute, showModal, resource, rootPath } = this.props;
    const route = `${rootPath}/${resource}/${id}/show`;
    if (redirects.edit === 'modal') {
      showModal(route);
    } else {
      pushRoute(route);
    }
  };

  gotoCreatePage = () => {
    const { redirects, pushRoute, showModal, resource, rootPath } = this.props;
    const route = `${rootPath}/${resource}/create`;
    if (redirects.create === 'modal') {
      showModal(route);
    } else {
      pushRoute(route);
    }
  };

  render() {
    return (
      <RestListComponent
        {...this.props}
        gotoEditPage={this.gotoEditPage}
        gotoCreatePage={this.gotoCreatePage}
        gotoShowPage={this.gotoShowPage}
        retrieveList={this.retrieveList}
      />
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    loading: getLoading(state, props.resource),
    resourceData: getResources(state, props.resource),
    resourceFilter: getFilters(state, props.resource),
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    retrieveList: (filter, isRefresh) => {
      return dispatch(
        retrieveList(
          props.resource,
          {
            ...props.initialFilter,
            ...filter,
          },
          isRefresh,
        ),
      );
    },
    customQuery: (id, queryUrl, data, isChangeToEdit) =>
      dispatch(customQuery(props.resource, id, queryUrl, data, isChangeToEdit)),
    updateRecord: (id, data, isChangeToEdit) =>
      dispatch(editRecord(props.resource, id, data, isChangeToEdit)),
    deleteItem: id => dispatch(deleteRecord(props.resource, id)),
    pushQuery: filter => dispatch(push(`${props.rootPath}/${props.resource}?${getSearch(filter)}`)),
    showModal: data => dispatch(showModalAction(data)),
    pushRoute: data => dispatch(push(data)),
  };
};

const ConnectRestList = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RestList);

RestList.propTypes = {
  location: PropTypes.object,
  pushQuery: PropTypes.func,
  retrieveList: PropTypes.func,
  initialFilter: PropTypes.object,
  resource: PropTypes.string,
  redirects: PropTypes.object,
  pushRoute: PropTypes.func,
  showModal: PropTypes.func,
  rootPath: PropTypes.string,
};

ConnectRestList.defaultProps = {
  rootPath: '',
  redirects: {
    edit: 'modal',
    create: 'modal',
  },
};

export default ConnectRestList;
