import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withTheme } from 'styled-components';
import HomeWrapper from './style';
import LayoutWrapper from '../../components/utility/LayoutWrapper';

class Home extends Component {
  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    return (
      <LayoutWrapper>
        <HomeWrapper />
      </LayoutWrapper>
    );
  }
}

Home.propTypes = {
  theme: PropTypes.object,
};

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = () => {
  return {};
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withTheme(Home));
