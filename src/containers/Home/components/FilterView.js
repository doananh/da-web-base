import React, { Component } from 'react';
import { Button, Checkbox, Form, Row, Col } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FilterViewWrapper } from './FilterView.styles';
import { BOOKINGS_TYPES, CHECKIN_STATUS } from '../../../config/constants';
import { setBookingFilter as setBookingFilterAction } from '../../../redux/bookings/actions';
import IntlMessages from '../../../components/utility/IntlMessages';
import RestSelect from '../../../components/RestInput/RestSelect';
import RestAutoComplete from '../../../components/RestInput/RestAutoComplete';
import ReferenceInput from '../../rest/ReferenceInput';
import { getRooms, getPackageTypes } from '../../../redux/configs/selectors';

class FilterView extends Component {
  onChangeTab = value => () => {
    this.props.setBookingFilter({ currentTab: value });
  };

  onChangeCheckinType = value => () => {
    const { filters, setBookingFilter } = this.props;
    setBookingFilter({
      checkinTypes: {
        ...filters.checkinTypes,
        [value]: !filters.checkinTypes[value],
      },
    });
  };

  onChangeFilter = prop => value => {
    const { setBookingFilter } = this.props;
    setBookingFilter({
      [prop]: value,
    });
  };

  onChangeCustomer = e => {
    if (e === '') {
      const { setBookingFilter } = this.props;
      setBookingFilter({
        userId: null,
      });
    }
  };

  render() {
    const { filters, rooms, packageTypes, form } = this.props;
    return (
      <Form>
        <FilterViewWrapper>
          <Button
            onClick={this.onChangeTab(BOOKINGS_TYPES[0])}
            className={`btn ${filters.currentTab === BOOKINGS_TYPES[0] ? 'btnSelected' : ''}`}
          >
            <IntlMessages id="text.today" />
          </Button>
          <Button
            onClick={this.onChangeTab(BOOKINGS_TYPES[1])}
            className={`btn ${filters.currentTab === BOOKINGS_TYPES[1] ? 'btnSelected' : ''}`}
          >
            <IntlMessages id="text.upcoming" />
          </Button>
          <Checkbox
            checked={!!filters.checkinTypes[CHECKIN_STATUS[0]]}
            onChange={this.onChangeCheckinType(CHECKIN_STATUS[0])}
            size="large"
          >
            <IntlMessages id="text.waitingCheckin" />
          </Checkbox>
          <Checkbox
            checked={!!filters.checkinTypes[CHECKIN_STATUS[1]]}
            onChange={this.onChangeCheckinType(CHECKIN_STATUS[1])}
            size="large"
          >
            <IntlMessages id="text.waitingCheckout" />
          </Checkbox>
          <Checkbox
            checked={!!filters.checkinTypes[CHECKIN_STATUS[2]]}
            onChange={this.onChangeCheckinType(CHECKIN_STATUS[2])}
            size="large"
          >
            <IntlMessages id="text.completed" />
          </Checkbox>
          <div className="filterInputWrapper">
            <Row gutter={16}>
              <Col md={6} xs={12}>
                <ReferenceInput source="name" form={form} searchKey="name" reference="users">
                  <RestAutoComplete
                    form={form}
                    className="filterInput"
                    valueProp="id"
                    titleProp="name"
                    placeholder="form.customerName"
                    onSelect={this.onChangeFilter('userId')}
                    onChange={this.onChangeCustomer}
                  />
                </ReferenceInput>
              </Col>
              <Col md={6} xs={12}>
                <RestSelect
                  form={form}
                  className="filterInput"
                  ruleType="number"
                  resourceData={rooms}
                  source="roomId"
                  valueProp="id"
                  titleProp="name"
                  placeholder="form.room"
                  onChange={this.onChangeFilter('roomId')}
                />
              </Col>
              <Col md={6} xs={12}>
                <RestSelect
                  form={form}
                  className="filterInput"
                  ruleType="number"
                  resourceData={packageTypes}
                  source="packageTypeId"
                  valueProp="id"
                  titleProp="name"
                  placeholder="form.packageType"
                  onChange={this.onChangeFilter('packageTypeId')}
                />
              </Col>
            </Row>
          </div>
        </FilterViewWrapper>
      </Form>
    );
  }
}
FilterView.propTypes = {
  setBookingFilter: PropTypes.func,
  filters: PropTypes.object,
  rooms: PropTypes.array,
  packageTypes: PropTypes.array,
  form: PropTypes.object,
};

const FilterForm = Form.create()(FilterView);
const FilterUI = props => <FilterForm {...props} />;

function mapStateToProps(state) {
  return {
    filters: state.bookings.filters,
    rooms: getRooms(state),
    packageTypes: getPackageTypes(state),
  };
}

const mapDispatchToProps = dispatch => {
  return {
    setBookingFilter: data => dispatch(setBookingFilterAction(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FilterUI);
