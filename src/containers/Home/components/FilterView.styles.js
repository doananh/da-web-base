import styled from 'styled-components';
import { palette } from 'styled-theme';

export const FilterViewWrapper = styled.div`
  margin-bottom: 20px;
  margin-top: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  .btn {
    width: 120px;
    border: 1px solid ${palette('text', 5)};
    border-radius: 20px;
    background: transparent;
    color: ${palette('text', 5)};
    margin-right: 20px;
    &:hover,
    &:active,
    &:focus {
      border: 1px solid ${palette('primary', 0)};
      color: ${palette('primary', 0)};
      background: transparent;
    }
  }
  .btnSelected {
    border: 1px solid ${palette('primary', 0)};
    color: ${palette('primary', 0)};
    background: transparent;
  }
  .ant-checkbox-wrapper {
    color: white;
    margin-right: 20px;
  }
  .selectRoomId {
    width: 200px;
  }
  .filterInputWrapper {
    display: inline-block;
    flex: 1;
    padding-top: 15px;
  }
`;
