import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getCurentUser, updateUser } from '../../redux/login/actions';
import { uploadUserAvatar } from '../../redux/user/actions';

class Profile extends Component {
  componentDidMount() {
    const { getProfile } = this.props;
    getProfile();
  }

  render() {
    return <div />;
  }
}
Profile.propTypes = {
  getProfile: PropTypes.func,
};

const mapStateToProps = state => {
  return {
    userInfo: state.login.data,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getProfile: () => dispatch(getCurentUser()),
    uploadAvatar: data => dispatch(uploadUserAvatar(props.userInfo.id, data)),
    updateUser: data => dispatch(updateUser(data)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);
