import React, { Component, lazy } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Redirect } from 'react-router-dom';
import { Layout } from 'antd';
import { Debounce } from 'react-throttle';
import WindowResizeListener from 'react-window-size-listener';
import Topbar from '../../containers/common/Topbar';
import Sidebar from '../../containers/common/Sidebar';
import Footer from '../../containers/common/Footer';
import AuthorizeRoute from '../subRoute/AuthorizeRoute';
import { siteConfig } from '../../config';
import appActions from '../../redux/app/actions';
import AppHolder from './style';
import ModalRoute from '../ModalRoute';

const { Content } = Layout;
const toggleAllApp = appActions.toggleAll;

const restRoutes = [
  //
];

// Pages
const authorizedRoutes = [
  {
    path: '/',
    component: lazy(() => import('../../containers/Home')),
    exact: true,
  },
  {
    path: '/profile',
    component: lazy(() => import('../../containers/Profile')),
    exact: true,
  },
  ...restRoutes,
  {
    path: '/config/:resource',
    component: lazy(() => import('../../containers/Config')),
  },
];

class PrivateRoute extends Component {
  componentDidMount() {}

  render() {
    const { match, toggleAll, location } = this.props;
    return (
      <div className="authorized-layout">
        <AppHolder>
          <Layout style={{ height: '100vh', overflow: 'hidden' }}>
            <Debounce time="1000" handler="onResize">
              <WindowResizeListener
                onResize={windowSize => toggleAll(windowSize.windowWidth, windowSize.windowHeight)}
              />
            </Debounce>
            <Topbar />
            <Layout style={{ flexDirection: 'row', overflow: 'hidden' }}>
              <Sidebar />
              <Layout
                className="isoContentMainLayout"
                style={{
                  overflow: 'auto',
                  overflowX: 'hidden',
                }}
              >
                <Content
                  className="isomorphicContent"
                  style={{
                    overflow: 'auto',
                    overflowX: 'hidden',
                    flexDirection: 'column',
                    flex: 1,
                    display: 'flex',
                  }}
                >
                  <div style={{ flex: 1 }}>
                    <Switch>
                      {authorizedRoutes.map(({ path, component, exact, role }) => {
                        const fullPath = match.path === '/' ? path : match.path + path;
                        return (
                          <AuthorizeRoute
                            match={match}
                            exact={exact}
                            path={fullPath}
                            component={component}
                            role={role}
                            key={fullPath}
                          />
                        );
                      })}
                      <Redirect
                        to={
                          match.path === '/'
                            ? authorizedRoutes[0].path
                            : match.path + authorizedRoutes[0].path
                        }
                      />
                    </Switch>
                  </div>
                  <ModalRoute location={location} match={match} />
                  <Footer>{siteConfig.footerText}</Footer>
                </Content>
              </Layout>
            </Layout>
          </Layout>
        </AppHolder>
      </div>
    );
  }
}

PrivateRoute.propTypes = {
  match: PropTypes.object,
  toggleAll: PropTypes.func,
  location: PropTypes.object,
};

const mapDispatchToProps = dispatch => {
  return {
    toggleAll: (width, height) => {
      dispatch(toggleAllApp(width, height));
    },
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(PrivateRoute);
