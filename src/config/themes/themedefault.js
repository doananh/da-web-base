const theme = {};
const PRIMARY_COLOR = '#5d4ec2';

theme.palette = {
  background: [
    '#f7f8fb', // 0: background
    '#fff', // 1: backgroundContent
    '#efeff0', // 2: background input
  ],
  primary: [
    PRIMARY_COLOR, // 0: Default
    '#3A78F5', // 1: Darken 4%
    '#49629a', // 2: Darken 5%
    'rgba(68, 130, 255, 0.2)', // 3: Fade 20%
    '#4C8AFF', // 4: Lighten 3%
    'rgba(68, 130, 255, 0.75)', // 5: Fade 75%
    '#6AA8FF', // 6: Lighten 15%
    '#e9f7fd', // 7: Lighten 12%
    '#3F7DFA', // 8: Darken 2%
    '#3369e7', // 9: Algolia color
    '#5896FF', // 10: Lighten 8%
    '#2b69e6', // 11:
    '#236cfe', // 12: darken 10%
    '#4d88ff', // 13: Lighten 5%
  ],
  secondary: [
    '#27324b', // 0: DarkBlue
    '#f0f3fa', // 1: LightBluish
    '#788195', // 2: LightBlue
    '#E4E6E9', // 3: LightBluish Darken 5%
    '#364d79', // 4:
    '#202739', // 5: DarkBlue Darken 5%
    '#f5f6f8', // 6: LighterBluish
    '#e9ebf1', // 7: DarkBluish
    '#F6F8FB', // 8: LighterBluish Lighten 2%
    '#E9EBEE', // 9: LighterBluish Darken 3%
    '#1a1a1a', // 10: Sidebar submenu select
  ],
  color: [
    '#f5a623', // 0: Orange
    '#665ca7', // 1: Purple
    '#F75D81', // 2: Pink
    '#4fcea2', // 3: LimeGreen
    '#2d7fd3', // 4: BlueShade
    '#FFCA28', // 5: Yellow
    '#F2BD1B', // 6: Yellow Darken 5%
    '#3b5998', // 7: Facebook
    '#344e86', // 8: Facebook Darken 5%
    '#dd4b39', // 9: Google Plus
    '#d73925', // 10: Google Plus Darken 5%
    '#e14615', // 11: Auth0
    '#ca3f13', // 12: Auth0
    '#e0364c', // 13: themeColor--AlizarinCrimson
    '#e46060', // 14: notification
    '#3ebac2', // 15: dark blue
    '#96959a', // 16: grey
    '#3b3852', // 17: dark grey
  ],
  warning: [
    '#ffbf00', // 0: Warning
  ],
  success: [
    '#00b16a', // 0: Success
  ],
  error: [
    '#f64744', // 0: Error
    '#EC3D3A', // 1: Darken 4%
    '#FF5B58', // 2: Lighten 8%
  ],
  grayscale: [
    '#b7b6c2',
    '#bababa', // 0: GreyShade
    '#c1c1c1', // 1: GreyDark
    '#D8D8D8', // 2: Grey
    '#f1f1f1', // 3: GreyAlt
    '#F3F3F3', // 4: GreyLight
    '#fafafa', // 5: DarkWhite
    '#F9F9F9', // 6: DarkerWhite
    '#fcfcfc', // 7: #fff Darken 1%
    '#eeeeee', // 8:
    '#fbfbfb', // 9:
    '#f5f5f5', // 10:
    '#f7f8f9', // 11: today-highlight-bg
  ],
  text: [
    '#262626', // 0: primary text
    '#4a4a4a', // 1: secondary text
    '#969696', // 2: sub Text, gray
    '#a1a0ad', // 3: placeholder text
    '#bbbbbb', // 4: disable text
    '#000', // 5: super dark
    '#0992d0', // 6: highlight text
  ],
  border: [
    'rgba(212, 210, 244, 0.5)', // 0: Border
    '#e9e9e9', // 1: BorderDark
    '#ebebeb', // 2: BorderLight
    '#d3d3d3', // 3:
    'rgba(228, 228, 228, 0.65)', // 4:
  ],
  calendar: [
    '#905', // 0:
    '#690', // 1:
    '#a67f59', // 2:
    '#07a', // 3:
    '#dd4a68', // 4:
    '#e90', // 5:
  ],
  sidebar: [
    '#fff', // 0: background
    '#fff', // 1: selected background
    '#a1a0ad', // 2: text color
    '#262626', // 3: selected text color
    '#262626', // 4: hover text
  ],
  table: [
    'rgba(212, 210, 244, 0.3)', // 0: header background
    '#fff', // 1: item background
    '#d1d1d1', // 2: hover items
    '#000', // 3: hover text
    PRIMARY_COLOR, // 4: active pagging
    '#fff', // 5: active pagging text
  ],
};

theme.fonts = {
  primary: 'Roboto',
  pre: 'Consolas, Liberation Mono, Menlo, Courier, monospace',
};

theme.fontWeights = {
  thin: 100, // Thin
  utraLight: 200, // Ultra Light
  light: 300, // Light
  regular: 400, // Regular
  medium: 500, // Medium
  semibold: 600, // Semibold
  bold: 700, // Bold
  heavy: 800, // Heavy
  black: 900, // Black
};

module.exports = theme;
