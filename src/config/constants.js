import vi from '../assets/images/flag/vi.png';
import en from '../assets/images/flag/en.png';

export const LANGUAGE = {
  text: {
    vi: 'Tiếng Việt',
    en: 'Tiếng Anh',
  },
  icon: {
    vi,
    en,
  },
};

export const APPROVE_STATUS = [
  {
    vi: 'Đã duyệt',
    value: true,
    en: 'Approved',
    color: 'green',
  },
  {
    value: false,
    vi: 'Chưa duyệt',
    en: 'Disapproved',
    color: '#03a9f4',
  },
];

export const GENDER = [
  {
    id: 'male',
    name: {
      en: 'Male',
      vi: 'Nam',
    },
  },
  {
    id: 'female',
    name: {
      en: 'Female',
      vi: 'Nữ',
    },
  },
];

export const REVIEW_LEVELS = [
  {
    id: 1,
    text: {
      en: 'At least 1 star',
      vi: 'Ít nhất 1 sao',
    },
  },
  {
    id: 2,
    text: {
      en: 'At least 2 star',
      vi: 'Ít nhất 2 sao',
    },
  },
  {
    id: 3,
    text: {
      en: 'At least 3 star',
      vi: 'Ít nhất 3 sao',
    },
  },
  {
    id: 4,
    text: {
      en: 'At least 4 star',
      vi: 'Ít nhất 4 sao',
    },
  },
  {
    id: 5,
    text: {
      en: 'At least 5 star',
      vi: 'Ít nhất 5 sao',
    },
  },
];

export const USER_STATUS = [
  {
    id: 1,
    data: 'true',
    text: {
      en: 'Locked',
      vi: 'Khoá',
    },
  },
  {
    id: 1,
    data: 'false',
    text: {
      en: 'Unlock',
      vi: 'Không Khoá',
    },
  },
];

export const MEMBER_STATUS = [
  {
    id: 1,
    data: 'false',
    text: {
      en: 'Locked',
      vi: 'Khoá',
    },
  },
  {
    id: 1,
    data: 'true',
    text: {
      en: 'Unlock',
      vi: 'Không Khoá',
    },
  },
];

export const ACTIVE_TYPES = [
  {
    id: 1,
    data: 'false',
    text: {
      en: 'deactive',
      vi: 'Không hoạt động',
    },
  },
  {
    id: 1,
    data: 'true',
    text: {
      en: 'active',
      vi: 'Đang hoạt động',
    },
  },
];

export const MEMBER_ROLES = [
  {
    id: 1,
    text: {
      en: 'Admin',
      vi: 'Admin',
    },
  },
  {
    id: 2,
    text: {
      en: 'Staff',
      vi: 'Nhân viên',
    },
  },
  {
    id: 3,
    text: {
      en: 'Member',
      vi: 'Khách hàng',
    },
  },
];

export const LANGUAGES = [
  {
    id: 'en',
    text: {
      en: 'English',
      vi: 'Tiếng Anh',
    },
  },
  {
    id: 'vi',
    text: {
      en: 'Vietnamese',
      vi: 'Tiếng Việt',
    },
  },
];

export const GENDERS = [
  {
    id: 'Male',
    text: {
      en: 'Male',
      vi: 'Nam',
    },
  },
  {
    id: 'Female',
    text: {
      en: 'Female',
      vi: 'Nữ',
    },
  },
];

export const BOOKING_STATUS = [
  { id: 'INPROGRESS', name: 'INPROGRESS', requestSuffixUrl: 'inprogress' },
  { id: 'COMPLETED', name: 'COMPLETED', requestSuffixUrl: 'complete' },
  { id: 'CANCELLED', name: 'CANCELLED', requestSuffixUrl: 'cancel' },
  { id: 'PENDING', name: 'PENDING' },
];

export const HOME_TAB = [
  { id: 'todayBooking', title: 'Today' },
  { id: 'pendingBooking', title: 'Upcoming' },
];

export const MOMENT_CODE = {
  daily: 'd',
  hourly: 'h',
  weekly: 'w',
  monthly: 'M',
};

export const TRANSACTION_TYPE = [
  { id: 'INCOME', name: { en: 'Income', vi: 'Thu vào' } },
  { id: 'EXPENSE', name: { en: 'Expense', vi: 'Chi ra' } },
];

export const PACKAGE_TYPES_TIME_UNIT = {
  hourly: 'hours',
  daily: 'days',
  weekly: 'weeks',
  monthly: 'month',
};
export const BOOKINGS_TYPES = ['todayBooking', 'pendingBooking'];
export const CHECKIN_STATUS = ['watingCheckin', 'waitingCheckout', 'completed'];
export const DISCOUNT_UNIT = [{ text: '%', id: 'percent' }, { text: 'VND', id: 'number' }];
