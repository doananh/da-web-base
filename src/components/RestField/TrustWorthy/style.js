import styled from 'styled-components';

const TrustWorthyWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  .progress {
    width: 120px;
  }
  .text {
    padding-left: 10px;
  }
`;

export default TrustWorthyWrapper;
