import styled from 'styled-components';

const TagsStackWrapper = styled.div`
  padding: 1px;
  display: flex;
  flex-wrap: wrap;
  .tag-item {
    i {
      font-size: 25px;
    }
  }
`;

export default TagsStackWrapper;
