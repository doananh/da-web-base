import styled from 'styled-components';
import { Tag } from 'antd';

const TagWrapper = styled(Tag)`
  color: ${props => props.theme && props.theme.color} !important;
  margin-bottom: 5px !important;
  font-size: 13px !important ;
  height: 25px !important;
  letter-spacing: 0.6pt !important;
  border-radius: 2pt !important;
  padding-left: 11pt !important;
  padding-right: 11pt !important;
`;

export default TagWrapper;
