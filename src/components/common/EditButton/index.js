import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from 'antd';

import IntlMessages from '../../utility/IntlMessages';
import Icomoon from '../../../assets/icomoon';
import { ButtonWrapper } from './styles';

const EditButton = props => {
  return (
    <ButtonWrapper
      className={props.className}
      role="button"
      onClick={() => props.onClick(props.record ? props.record.id : '')}
    >
      <Tooltip title={<IntlMessages id="tooltip.edit" />}>
        <Icomoon type="ic-edit" />
      </Tooltip>
    </ButtonWrapper>
  );
};

EditButton.propTypes = {
  onClick: PropTypes.func,
  record: PropTypes.object,
  className: PropTypes.string,
};

EditButton.defaultProps = {
  source: 'edit',
};

export default EditButton;
