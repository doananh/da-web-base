import React from 'react';
import { Spin } from 'antd';
import { Container } from './style';

const WayPointLoading = () => (
  <Container>
    <Spin />
  </Container>
);

export default WayPointLoading;
