import styled from 'styled-components';

export const TitleWrapper = styled('span')`
  padding-right: 10px;
  height: 25px;
`;
