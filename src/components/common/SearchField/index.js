import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'antd';
import { SearchWrapper } from './styles';

const Search = props => {
  return (
    <div>
      <SearchWrapper
        placeholder={props.placeholder}
        suffix={<Icon type="search" style={{ color: 'rgba(0,0,0,.25)', fontSize: 30 }} />}
        onChange={props.onChange}
      />
    </div>
  );
};

Search.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
};
export default Search;
