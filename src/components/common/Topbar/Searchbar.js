import React, { Component } from 'react';
import { InputSearch } from '../../uielements/Input';

class Searchbar extends Component {
  componentDidMount() {
    setTimeout(() => {
      try {
        document.getElementById('InputTopbarSearch').focus();
      } catch (e) {
        //
      }
    }, 200);
  }

  render() {
    return <InputSearch id="InputTopbarSearch" placeholder="Enter search text" />;
  }
}

export default Searchbar;
