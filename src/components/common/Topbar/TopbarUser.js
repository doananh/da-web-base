import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Popover } from 'antd';
import IntlMessages from '../../utility/IntlMessages';
import userpic from '../../../assets/images/user1.png';
import TopbarDropdownWrapper from './TopbarDropdown.style';
import Text from '../Text';

class TopbarUser extends Component {
  constructor(props) {
    super(props);
    this.handleVisibleChange = this.handleVisibleChange.bind(this);
    this.hide = this.hide.bind(this);
    this.state = {
      visible: false,
    };
  }

  hide() {
    this.setState({ visible: false });
  }

  handleVisibleChange() {
    this.setState({ visible: !this.state.visible });
  }

  render() {
    const { userInfo } = this.props;
    const content = (
      <TopbarDropdownWrapper className="isoUserDropdown">
        <a href="/profile" className="isoDropdownLink">
          <IntlMessages id="topbar.setting" />
        </a>
        {/* <a className="isoDropdownLink">
          <IntlMessages id="sidebar.feedback" />
        </a>
        <a className="isoDropdownLink">
          <IntlMessages id="topbar.help" />
        </a> */}
        <div role="button" onClick={this.props.logout} className="isoDropdownLink">
          <IntlMessages id="topbar.logout" />
        </div>
      </TopbarDropdownWrapper>
    );

    return (
      <Popover
        content={content}
        trigger="click"
        visible={this.state.visible}
        onVisibleChange={this.handleVisibleChange}
        placement="bottomLeft"
      >
        <div className="isoImgWrapper">
          <div className="isoUserInfoWrapper">
            <Text type="h5" align="right">
              {userInfo ? userInfo.fullName : ''}
            </Text>
            <Text type="bodyGray" align="right">
              Admin
            </Text>
          </div>
          <img alt="user" src={userpic} />
          <span className="userActivity online" />
        </div>
      </Popover>
    );
  }
}

TopbarUser.propTypes = {
  logout: PropTypes.func,
  userInfo: PropTypes.object,
};

export default TopbarUser;
