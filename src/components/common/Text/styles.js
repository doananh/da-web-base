import styled from 'styled-components';
import { palette, key } from 'styled-theme';

export const TextWrapper = styled.span`
  fontfamily: ${key('fonts.primary')};
  color: ${palette('text', 0)};
  line-height: 20px;
  .bigTitle {
    /*64 */
    font-size: 64px;
    line-height: 70px;
    font-weight: ${key('fontWeights.medium')};
    color: ${palette('text', 1)};
  }
  .h1 {
    /* 36 - bold */
    font-size: 36px;
    line-height: 42px;
    font-weight: ${key('fontWeights.bold')};
    color: ${palette('text', 1)};
  }
  .h1SemiBold {
    /* 36 - bold */
    font-size: 36px;
    line-height: 42px;
    font-weight: ${key('fontWeights.semibold')};
    color: ${palette('text', 1)};
  }
  .h2 {
    /* 30 - medium */
    font-size: 30px;
    line-height: 42px;
    font-weight: ${key('fontWeights.medium')};
    color: ${palette('text', 0)};
  }
  .h3 {
    /* 24 - semibold */
    font-size: 24px;
    line-height: 32px;
    font-weight: ${key('fontWeights.semibold')};
    color: ${palette('text', 0)};
  }
  .h4 {
    /* 20 - semibold */
    font-size: 20px;
    line-height: 28px;
    font-weight: ${key('fontWeights.semibold')};
    color: ${palette('text', 0)};
  }
  .h4White {
    /* 20 - semibold */
    font-size: 20px;
    line-height: 28px;
    font-weight: ${key('fontWeights.semibold')};
    color: white;
  }
  .h5Gray {
    /* 16 - semibold - gray */
    font-size: 16px;
    line-height: 24px;
    font-weight: ${key('fontWeights.semibold')};
    color: ${palette('text', 2)};
  }
  .h5 {
    /* 16 - semibold */
    font-size: 16px;
    line-height: 24px;
    font-weight: ${key('fontWeights.regular')};
    color: ${palette('text', 5)};
  }
  .buttonWhite {
    /* 14 - medium */
    font-size: 14px;
    line-height: 22px;
    font-weight: ${key('fontWeights.medium')};
    color: white;
  }
  .button {
    /* 14 */
    font-size: 14px;
    line-height: 22px;
    font-weight: ${key('fontWeights.regular')};
    color: ${palette('text', 1)};
  }
  .buttonGray {
    /* 14 */
    font-size: 14px;
    line-height: 22px;
    font-weight: ${key('fontWeights.regular')};
    color: ${palette('text', 4)};
  }
  .bodyGray {
    /* 14 - gray */
    font-size: 14px;
    line-height: 22px;
    font-weight: ${key('fontWeights.regular')};
    color: ${palette('text', 3)};
  }
  .bodyHighLight {
    /* 14 - blue */
    font-size: 14px;
    line-height: 22px;
    font-weight: ${key('fontWeights.regular')};
    color: ${palette('text', 6)};
  }
  .body {
    /* 14 */
    font-size: 14px;
    line-height: 22px;
    font-weight: ${key('fontWeights.regular')};
    color: ${palette('text', 0)};
  }
  .inputDisabled {
    /* 12 - disabled */
    font-size: 12px;
    line-height: 20px;
    font-weight: ${key('fontWeights.regular')};
    color: ${palette('text', 4)};
  }
  .input {
    /* 12 */
    font-size: 12px;
    line-height: 20px;
    font-weight: ${key('fontWeights.regular')};
    color: ${palette('text', 0)};
  }
  .inputWhite {
    /* 12 - white */
    font-size: 12px;
    line-height: 20px;
    font-weight: ${key('fontWeights.regular')};
    color: white;
  }
  .text {
    /*12 */
    font-size: 12px;
    line-height: 20px;
    font-weight: ${key('fontWeights.regular')};
    color: ${palette('text', 1)};
  }
  .smallText {
    /*10 */
    font-size: 10px;
    line-height: 15px;
    font-weight: ${key('fontWeights.regular')};
    color: ${palette('text', 1)};
  }
  /* txtUnderline */
  .txtUnderline {
    text-decoration-line: 'underline';
  }
  .light {
    font-weight: ${key('fontWeights.light')};
  }
  .medium {
    font-weight: ${key('fontWeights.medium')};
  }
  .bold {
    font-weight: ${key('fontWeights.bold')};
  }
  .semiBold {
    font-weight: ${key('fontWeights.semibold')};
  }
  .left {
    text-align: left;
  }
  .center {
    text-align: center;
  }
  .right {
    text-align: right;
  }
  .inline {
    display: inline;
  }
`;
