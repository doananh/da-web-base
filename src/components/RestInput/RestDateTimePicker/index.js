import React from 'react';
import PropTypes from 'prop-types';
import FormDateTimePicker from '../../form/FormDateTimePicker';
import { getRecordData } from '../../../helpers/Tools';

const RestFormDateTimePicker = props => (
  <FormDateTimePicker {...props} defaultValue={getRecordData(props.record, props.source)} />
);

RestFormDateTimePicker.propTypes = {
  source: PropTypes.string,
  record: PropTypes.object,
};

export default RestFormDateTimePicker;
