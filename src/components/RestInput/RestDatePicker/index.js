import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import FormDatePicker from '../../form/FormDatePicker';
import { getRecordData } from '../../../helpers/Tools';

const RestFormDateInput = props => (
  <FormDatePicker
    {...props}
    defaultValue={moment(getRecordData(props.record, props.source) || props.defaultValue)}
  />
);

RestFormDateInput.propTypes = {
  source: PropTypes.string,
  record: PropTypes.object,
  defaultValue: PropTypes.any,
};

export default RestFormDateInput;
