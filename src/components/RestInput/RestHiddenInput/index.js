import React from 'react';
import PropTypes from 'prop-types';
import FormInput from '../../form/FormInput';
import { getRecordData } from '../../../helpers/Tools';

const RestHiddenInput = props => {
  return (
    <div style={{ display: 'none' }}>
      <FormInput
        {...props}
        defaultValue={props.defaultValue || getRecordData(props.record, props.source)}
      />
    </div>
  );
};

RestHiddenInput.propTypes = {
  source: PropTypes.string,
  record: PropTypes.object,
  defaultValue: PropTypes.string,
};

export default RestHiddenInput;
