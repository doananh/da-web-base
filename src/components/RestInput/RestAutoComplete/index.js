import React from 'react';
import PropTypes from 'prop-types';
import { getRecordData } from '../../../helpers/Tools';
import FormAutoComplete from '../../form/FormAutoComplete';

const RestAutoComplete = props => {
  return (
    <FormAutoComplete
      {...props}
      defaultValue={props.defaultValue || getRecordData(props.record, props.source)}
    />
  );
};

RestAutoComplete.propTypes = {
  source: PropTypes.string,
  record: PropTypes.object,
  defaultValue: PropTypes.string,
};

export default RestAutoComplete;
