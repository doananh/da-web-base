import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';
import { ButtonWrapper, FooterButtonRowWrapper } from './styles';
import IntlMessages from '../../utility/IntlMessages';

const ButtonRow = props => {
  const { loading, onBack, handleSubmit, type, showModal } = props;
  if (showModal) {
    return (
      <FooterButtonRowWrapper>
        <ButtonWrapper
          onClick={() => {
            handleSubmit();
          }}
          type="primary"
          loading={loading}
        >
          {<IntlMessages id={type === 'create' ? 'button.create' : 'button.save'} />}
        </ButtonWrapper>
        <span style={{ width: 20 }} />
        <ButtonWrapper
          onClick={() => {
            onBack();
          }}
        >
          {<IntlMessages id="button.cancel" />}
        </ButtonWrapper>
      </FooterButtonRowWrapper>
    );
  }
  return (
    <Row gutter={8} type="flex" justify="end" style={{ marginTop: 20 }}>
      <Col lg={12} md={0} sm={0} xs={24} />
      <Col lg={showModal ? 6 : 3} md={3} sm={12} xs={12}>
        <ButtonWrapper
          onClick={() => {
            handleSubmit();
          }}
          type="primary"
          loading={loading}
        >
          {<IntlMessages id={type === 'create' ? 'button.create' : 'button.save'} />}
        </ButtonWrapper>
      </Col>
      <Col md={0} sm={0} xs={24} />
      <Col lg={showModal ? 6 : 3} md={3} sm={12} xs={12}>
        <ButtonWrapper
          onClick={() => {
            onBack();
          }}
        >
          {<IntlMessages id="button.cancel" />}
        </ButtonWrapper>
      </Col>
    </Row>
  );
};

ButtonRow.propTypes = {
  onBack: PropTypes.func,
  showModal: PropTypes.bool,
  loading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  type: PropTypes.oneOf(['create', 'edit']),
};

ButtonRow.defaultProps = {
  type: 'edit',
};
export default ButtonRow;
