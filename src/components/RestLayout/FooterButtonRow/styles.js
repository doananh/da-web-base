import styled from 'styled-components';
import { Button } from 'antd';
import { palette } from 'styled-theme';

export const ButtonWrapper = styled(Button)`
  width: 100%;
  align-items: center;
`;

export const FooterButtonRowWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 20px;
  margin-bottom: 10px;
  .ant-btn {
    width: 140px;
    border-color: ${palette('primary', 0)};
  }
`;
