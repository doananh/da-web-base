import React from 'react';
import PropTypes from 'prop-types';
import { Input, Icon } from 'antd';
import IntlMessages from '../../utility/IntlMessages';
import { SearchInputWrapper } from './styles';

const SearchInput = ({ onTextSearch, defaultValue }) => {
  return (
    <SearchInputWrapper>
      <IntlMessages id="text.search">
        {placeholder => (
          <Input
            defaultValue={defaultValue}
            placeholder={placeholder}
            prefix={<Icon type="search" />}
            onPressEnter={e => onTextSearch(e.currentTarget.value)}
            className="input"
          />
        )}
      </IntlMessages>
    </SearchInputWrapper>
  );
};
SearchInput.propTypes = {
  onTextSearch: PropTypes.func,
  defaultValue: PropTypes.string,
};

export default SearchInput;
