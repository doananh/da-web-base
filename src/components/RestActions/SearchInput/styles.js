import styled from 'styled-components';
import { palette } from 'styled-theme';

export const SearchInputWrapper = styled.div`
  flex: 1;
  input {
    background: ${palette('background', 1)};
    box-shadow: 0 1px 10px -3px rgba(0, 0, 0, 0.15);
    width: 320px;
  }
  .ant-input {
    background: ${palette('background', 1)} !important;
  }
`;
