import React from 'react';
import PropTypes from 'prop-types';
import IntlMessages from '../../utility/IntlMessages';
import { ButtonWrapper } from './styles';

const EditButton = props => {
  return (
    <ButtonWrapper type="primary" onClick={props.gotoCreatePage}>
      <IntlMessages id={props.title} />
    </ButtonWrapper>
  );
};
EditButton.propTypes = {
  gotoCreatePage: PropTypes.func,
  title: PropTypes.string,
};

EditButton.defaultProps = {
  source: 'create',
  title: 'button.create',
};

export default EditButton;
