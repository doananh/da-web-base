import React from 'react';
import PropTypes from 'prop-types';
import { ButtonWrapper } from './styles';
import Icomoon from '../../../assets/icomoon';

const ExportExcelButton = props => {
  return (
    <ButtonWrapper onClick={props.exportExcel}>
      <Icomoon type="ic-excel" />
    </ButtonWrapper>
  );
};
ExportExcelButton.propTypes = {
  exportExcel: PropTypes.func,
};

ExportExcelButton.defaultProps = {
  source: 'create',
  exportExcel: () => {},
};

export default ExportExcelButton;
