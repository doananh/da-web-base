import React from 'react';
import PropTypes from 'prop-types';
import { siteConfig } from '../../config';

const Logo = ({ onClick, collapsed }) => {
  return (
    <div role="button" onClick={onClick}>
      <img
        src={siteConfig.siteName}
        style={{
          cursor: 'pointer',
          marginRight: 24,
          marginLeft: 20,
          lineHeight: 0,
          visibility: collapsed ? 'hidden' : 'visible',
        }}
        alt="kuuho logo"
      />
      <img
        src={siteConfig.siteIcon}
        style={{
          position: 'absolute',
          cursor: 'pointer',
          marginRight: 24,
          left: 20,
          lineHeight: 0,
          marginTop: 3,
        }}
        alt="kuuho logo"
      />
    </div>
  );
};

Logo.propTypes = {
  onClick: PropTypes.func,
  collapsed: PropTypes.bool,
};

export default Logo;
