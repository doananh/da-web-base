import styled from 'styled-components';
import { Modal } from 'antd';
import { palette } from 'styled-theme';

export const ModalWrapper = styled(Modal)`
  max-height: 90%;
  min-height: 30%;
  .ant-modal-header {
    background: ${palette('background', 1)};
    border-bottom: 1px solid ${palette('background', 0)};
    color: ${palette('primary', 0)};
  }
  .ant-modal-content {
    background: ${palette('background', 0)};
    padding-top: 60px;
  }
  .ant-modal-title {
    color: ${palette('primary', 0)};
    font-size: 35px;
  }
  .ant-modal-close,
  .ant-modal-close-icon {
    display: none;
  }
  .ant-input,
  .ant-select-selection,
  .ant-input-number,
  .ant-select-dropdown-menu-item,
  .ant-select-dropdown-menu,
  .ant-select-dropdown,
  .ant-select-clear-icon,
  .ant-select-dropdown-menu-vertical {
    background: ${palette('background', 1)};
    border: 1px solid ${palette('border', 0)};
    &:hover,
    &:focus,
    &:active {
      border: 1px solid ${palette('border', 0)};
    }
  }
  textarea {
    background: ${palette('background', 1)};
    border: none;
    &:hover,
    &:focus,
    &:active {
      border: 1px solid ${palette('border', 0)};
    }
  }
  .ant-select-selection__clear {
    background-color: transparent;
    color: white;
    border-radius: 5px;
  }
  .ant-select-arrow-icon {
    background-color: transparent;
  }
  * {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  .ant-modal-footer {
    border-top: 1px solid ${palette('background', 0)};
  }

  .ant-modal-body {
    padding: 10px 24px;
  }

  .ant-tabs-bar {
    border-bottom: none;
    padding-bottom: 10px;
    margin-bottom: 30px;
    border-bottom: 1px solid ${palette('background', 0)};
  }
  .ant-tabs-tab {
    color: ${palette('text', 5)};
  }
  .ant-list {
    margin-top: 20px;
    overflow: auto;
    max-height: 460px;
  }
  div::-webkit-scrollbar-thumb {
    border-radius: 3px !important;
    background: ${palette('secondary', 2)} !important;
    box-shadow: 1px 1px 2px 0 rgba(0, 0, 0, 0.5) !important;
  }
  div::-webkit-scrollbar-track {
    border-radius: 3px !important;
    background: ${palette('grayscale', 2)} !important;
  }
  div::-webkit-scrollbar-thumb:hover {
    border-radius: 3px !important;
    background: ${palette('secondary', 2)} !important;
    box-shadow: 1px 1px 2px 0 rgba(0, 0, 0, 0.5) !important;
  }
  div::-webkit-scrollbar {
    width: 6px;
    border-radius: 3px !important;
    background: ${palette('grayscale', 2)} !important;
  }
  .ant-list-split .ant-list-item {
    border-bottom: none;
    padding: 1px 0px;
  }
  .ant-list-empty-text {
    color: ${palette('text', 5)};
  }
  .modalTitle {
    background: ${palette('primary', 0)};
    text-align: center;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    height: 60px;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .ant-form-item {
    margin-bottom: 0px;
    margin-right: -1px;
  }
`;
