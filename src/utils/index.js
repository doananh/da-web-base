import unidecode from 'unidecode';
import { keyBy } from 'lodash';

export const onSearch = (data, keySearch) => {
  return (
    data &&
    unidecode(data)
      .toLowerCase()
      .search(unidecode(keySearch).toLowerCase()) !== -1
  );
};

export const formattedData = list => {
  return {
    data: keyBy(list, 'id'),
    ids: list.map(data => data.id),
  };
};
